<?php

/**
 * This file is part of the package demosplan.
 *
 * (c) 2010-present DEMOS plan GmbH, for more information see the license file.
 *
 * All rights reserved
 */

namespace demosplan\DemosPlanCoreBundle\Security\Authentication\Provider;

use demosplan\DemosPlanCoreBundle\Entity\User\AnonymousUser;
use demosplan\DemosPlanCoreBundle\Entity\User\User;
use demosplan\DemosPlanCoreBundle\Logic\User\UserService;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ApiUserProvider implements UserProviderInterface
{
    public function __construct(private readonly UserService $userService)
    {
    }

    /**
     * {@inheritDoc}
     *
     * @param string $login
     */
    public function loadUserByUsername($login): UserInterface
    {
        // avoid database call if anonymous user calls API
        if (User::ANONYMOUS_USER_NAME === $login) {
            return new AnonymousUser();
        }

        $user = $this->userService->findDistinctUserByEmailOrLogin($login);

        if (!$user instanceof User) {
            $user = new AnonymousUser();
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        return $user;
    }

    /**
     * {@inheritDoc}
     *
     * @param string $class
     */
    public function supportsClass($class): string
    {
        return User::class;
    }
}
