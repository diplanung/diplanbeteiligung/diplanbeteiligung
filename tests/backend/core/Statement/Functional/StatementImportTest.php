<?php

declare(strict_types=1);

/**
 * This file is part of the package demosplan.
 *
 * (c) 2010-present DEMOS plan GmbH, for more information see the license file.
 *
 * All rights reserved
 */

namespace Tests\Core\Statement\Functional;

use demosplan\DemosPlanCoreBundle\DataFixtures\ORM\TestData\LoadProcedureData;
use demosplan\DemosPlanCoreBundle\DataFixtures\ORM\TestData\LoadUserData;
use demosplan\DemosPlanCoreBundle\Entity\Report\ReportEntry;
use demosplan\DemosPlanCoreBundle\Entity\Statement\Statement;
use demosplan\DemosPlanCoreBundle\Logic\FileService;
use demosplan\DemosPlanCoreBundle\Logic\Procedure\CurrentProcedureService;
use demosplan\DemosPlanCoreBundle\Logic\Statement\XlsxStatementImport;
use Tests\Base\FunctionalTestCase;

class StatementImportTest extends FunctionalTestCase
{
    /**
     * @var XlsxStatementImport;
     */
    protected $sut;
    /**
     * @var FileService
     */
    private $fileService;

    public function setUp(): void
    {
        parent::setUp();

        $this->sut = self::$container->get(XlsxStatementImport::class);
        $this->fileService = self::$container->get(FileService::class);
    }

    public function testGenerateStatementsFromExcel(): void
    {
        self::markSkippedForCIElasticsearchUnavailable();

        $this->setProcedureAndLogin();
        $countBefore = $this->countEntries(Statement::class);
        $testFile = $this->getFileReference('statements_as_xlsx');
        $fileHash = $testFile->getHash();
        $file = $this->fileService->getFileInfo($fileHash);
        $this->sut->importFromFile($file);

        static::assertFalse($this->sut->hasErrors());
        static::assertCount(4, $this->sut->getCreatedStatements());
        $generatedStatementsAfter = $this->getEntries(Statement::class);
        // expect 8 new statement entries, because of the created original statements
        static::assertCount($countBefore + (4 * 2), $generatedStatementsAfter);
    }

    private function setProcedureAndLogin(): void
    {
        /** @var CurrentProcedureService $currentProcedureService */
        $currentProcedureService = self::$container->get(CurrentProcedureService::class);
        $currentProcedureService->setProcedure($this->getProcedureReference(LoadProcedureData::TESTPROCEDURE));
        $this->logIn($this->getUserReference(LoadUserData::TEST_USER_PLANNER_AND_PUBLIC_INTEREST_BODY));
    }

    public function testReportEntriesOnImportNewStatements(): void
    {
        self::markSkippedForCIElasticsearchUnavailable();

        $this->setProcedureAndLogin();
        $countBefore = $this->countEntries(ReportEntry::class);
        $testFile = $this->getFileReference('statements_as_xlsx');
        $fileHash = $testFile->getHash();
        $file = $this->fileService->getFileInfo($fileHash);
        $this->sut->importFromFile($file);

        static::assertFalse($this->sut->hasErrors());
        static::assertCount(4, $this->sut->getCreatedStatements());
        $generatedReportsAfter = $this->getEntries(ReportEntry::class);
        static::assertCount($countBefore + 4, $generatedReportsAfter);
    }

    public function testRollbackOnError(): void
    {
        self::markSkippedForCIElasticsearchUnavailable();

        $this->setProcedureAndLogin();
        $countBefore = $this->countEntries(Statement::class);
        $testFile = $this->getFileReference('statements_as_xlsx_including_an_error');
        $fileHash = $testFile->getHash();
        $file = $this->fileService->getFileInfo($fileHash);

        $this->sut->importFromFile($file);

        static::assertTrue($this->sut->hasErrors());
        static::assertCount(8, $this->sut->getErrorsAsArray());
        $generatedStatementsAfter = $this->getEntries(Statement::class);
        static::assertCount($countBefore + 0, $generatedStatementsAfter);
    }
}
